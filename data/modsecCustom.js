
'use strict'
let fs = require('fs')
let path = require('path');
let beautify = require("json-beautify");
const node_ssh = require('node-ssh');
const ssh = new node_ssh();
const readReplace = require('../lib/services')
const { TARGET_IP, INSTANCE_KEY } = require('./RULES_CONST')
const projectPath = path.resolve(__dirname, '..') + '/'
console.log(INSTANCE_KEY);

ssh.connect({
  host: TARGET_IP,
  username: 'centos',
  privateKey: INSTANCE_KEY,
}).then(resp => {
  console.log('SUCCESS!!');

  ssh.getFile(`${projectPath}data/rules/main.conf`, '/opt/cequence/appfirewall/conf/appfirewall/rules/custom/main.conf').then(function(Contents) {
    console.log("The File's contents were successfully downloaded")
  }, function(error) {
    console.log("Something's wrong at GET FIle")
    console.log(error)
  })
  const getUploadedFile = fs.readFileSync(`${projectPath}data/temp.json`, 'utf-8')
  const updatedConfiguration = readReplace(getUploadedFile);
  beautify(updatedConfiguration, null, 2, 100)
}).catch(err => {
  console.log(err, 'ERROR');
});
