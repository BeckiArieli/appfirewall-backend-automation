let path = require('path');
const projectPath = path.resolve(__dirname, '..') + '/'


//*******MAIN
const TARGET_IP =  process.env.TARGET_IP || "10.50.5.115"
const DETECTOR_IP =  process.env.DETECTOR_IP || "10.50.6.102"
const INSTANCE_KEY = process.env.INSTANCE_KEY ||`${projectPath}lib/beckiengcequencecom.pem`

exports.TARGET_IP = TARGET_IP
exports.DETECTOR_IP = DETECTOR_IP
exports.INSTANCE_KEY = INSTANCE_KEY


//******** Owasp

//******** Custom Rules
// const CUSTOM_RULES = [
//   {
//     id: 1,
//     name: "Schould block Accept-Lamnguage rule",
//     rule: ['SecRule "REQUEST_HEADERS:X-OPT" "@contains nikto" "id:30101,phase:1,redirect:honeytrap,status:200,msg:'Honey Trapp'"'],
//     status: 'ok'
//   },
//   {
//     id: 2,
//     name: "Schould block Accept-Language rule",
//     rule: ['SecRule "REQUEST_HEADERS:Accept-Language" "@contains fr-CH" "id:30102,phase:1,deny,status:405,msg:'Language'"'],
//     status: 'ok'
//   },
//   {
//     id: 3,
//     name: "Schould block Accept-Lamnguage rule",
//     rule: ['SecRule "TX:cq_true_client_ip" "@rx ^((1\.){3}([1-9]|[1-9]\d|[12]\d\d)|(1\.){2}([2-9]|[1-9]\d|[12]\d\d)\.([1-9]?\d|[12]\d\d)|1\.([2-9]|[1-9]\d|[12]\d\d)(\.([1-9]?\d|[12]\d\d)){2}|(10\.){3}(\d|10)|(10\.){2}\d\.([1-9]?\d|[12]\d\d)|10\.\d(\.([1-9]?\d|[12]\d\d)){2}|[2-9](\.([1-9]?\d|[12]\d\d)){3})$" "id:30103,phase:1,deny,status:333,msg:'Range !PS'"'],
//     status: 'off'
//   },
//   {
//     id: 4,
//     name: "Schould block Accept-Lamnguage rule",
//     rule: ['SecRule "REQUEST_COOKIES:myApp-token" "@contains 12345667" "id:30104,phase:1,deny,status:457,msg:'Multiple Cretireas'"'],
//     status: 'ok'
//   },
//   {
//     id: 5,
//     name: "Schould block Accept-Lamnguage rule",
//     rule: ['SecRule "REQUEST_COOKIES:myApp-token" "@contains 12345667" "id:30104,phase:1,deny,status:457,msg:'Multiple Cretireas'"'],
//     status: 'ok'
//   }
// ]


//******** MODSEC Rules
// const MODSEC_RULES = [
//   {
//     id:1,
//     name: 'MODSEC block cookies',
//     rule: 'SecRule "REQUEST_COOKIES:myApp-token" "@contains 12345667" "id:50101,phase:2,deny,status:457,msg:'MODSEC'"',
//     status: 'ok'
//   }
// ]

//******** LUA SCRIPT Rules
// const LUA_RULES = [
//   {
//     id: 1,
//     name: 'LUA',
//     rule: 'SecRuleScript "/opt/cequence/appfirewall/conf/appfirewall/rules/scripts/50102.lua" "id:50102,phase:1,deny,status:777,msg:'LUA'"',
//     status: 'ok'
//   }
// ]

const OWASP_PATH = '/opt/cequence/appfirewall/conf/appfirewall/rules/owasp/main.conf'
const CUSTOM_PATH = '/opt/cequence/appfirewall/conf/appfirewall/rules/custom/main.conf'
const MODSEC_PATH = '/opt/cequence/appfirewall/conf/appfirewall/rules/modsec/main.conf'
const SCRIPT_PATH = '/opt/cequence/appfirewall/conf/appfirewall/rules/scripts/main.conf'


/***********************************
*********** DOUBLE RULE ************

1. fingerPrint: 4143388522 (Posible Session Fixation)
2.  -H 'Accept-Language: pt-BR,pt;q=0.9,it;q=0.8'
3. IP begin with:
************************************
Query:
while [ true ]; do curl -ki -XPOST "https:/10.50.5.115/?phpsessid=asdfdasfadsads" -H "Host: live.emadisonisland.com" -H "Referer: https://localhost.attackersite.com/" -H "X-Forwarded-For: 10.10.10.41" -H 'Accept-Language: pt-BR,pt;q=0.9,it;q=0.8'; sleep 1 ;done

***********************************
Rule:
await api.get('/?phpsessid=asdfdasfadsads')
  .set('Referer', 'https://localhost.attackersite.com/')
  .expect(403)
  .then(res => {
    console.log('SUCCESS OWASP: block based on Possible Session Fixation Attack: SessionID Parameter Name with Off-Domain Referer 943110', res.status);
    assert(res.status === 403)
  })
sleep(1000)
********************************************/
