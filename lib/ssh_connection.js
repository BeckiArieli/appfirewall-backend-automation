'use strict'
let fs = require('fs')
var path = require('path');
var beautify = require("json-beautify");
const node_ssh = require('node-ssh');
const ssh = new node_ssh();
const readReplace = require('../lib/services')
const { TARGET_IP, DETECTOR_IP, INSTANCE_KEY } = require('../data/RULES_CONST')

module.export class SSH_CONNECTION {
  constructor(cb) {
    const projectPath = path.resolve(__dirname, '..') + '/'

    ssh.connect({
      host: TARGET_IP,
      username: 'centos',
      privateKey: INSTANCE_KEY,
    }).then(resp => {
      console.log('SUCCESS!!');
      console.log('CALLBACK executing');
      cb()

    }).catch(err => {
      console.log(err, 'ERROR');
    });
  }
}
