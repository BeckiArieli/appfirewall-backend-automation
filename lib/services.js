'use strict'
let fs = require('fs')

const readReplace = (file) => {
  let temp = JSON.parse(file)
  const result = {}
  for (let [key, value] of Object.entries(temp)) {
    if(key === 'MITIGATOR_ENABLED'){
      result[key] = false
    } else if(key === 'APPFIREWALL_ENABLED' ){
      result[key] = true
    } else {
      result[key] = value
      }
  }
  return result;
}
module.exports = readReplace
