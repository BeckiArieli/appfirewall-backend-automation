const curl = new (require( 'curl-request' ))();
const node_ssh = require('node-ssh');
const ssh = new node_ssh();
var path = require('path');
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const { TARGET_IP, DETECTOR_IP, INSTANCE_KEY } = require('../data/RULES_CONST')
const projectPath = path.resolve(__dirname, '..') + '/'
//curl -XDELETE  10.50.17.34:9200/appfirewall
curl
.delete(`${DETECTOR_IP}:9200/appfirewall`)
.then(({statusCode, body, headers}) => {
    console.log(statusCode, body, headers)
})
.catch((e) => {
    console.log(e);
});

ssh.connect({
  host: DETECTOR_IP,
  username: 'centos',
  privateKey: INSTANCE_KEY,
}).then(resp => {
  console.log('SUCCESS!!');
  //stop analytics
  ssh.execCommand('/opt/velocity-manager-host/bin/velocity-manager stop -s kafka ')
  .then(function(result1) {
    sleep(10000)
    console.log(result1);
  })
  ssh.execCommand('/opt/velocity-manager-host/bin/velocity-manager stop -s analytics ')
  .then(function(result2) {
    sleep(10000)
    console.log(result2);
  })
  ssh.execCommand('/opt/velocity-manager-host/bin/velocity-manager stop -s prometheus ')
  .then(function(result3) {
    sleep(10000)
    console.log(result3);
  })
  //start analytics
  ssh.execCommand('/opt/velocity-manager-host/bin/velocity-manager start -s kafka ')
  .then(function(result1) {
    sleep(10000)
    console.log(result1);
  })
  ssh.execCommand('/opt/velocity-manager-host/bin/velocity-manager start -s analytics ')
  .then(function(result2) {
    sleep(10000)
    console.log(result2);
  })
  ssh.execCommand('/opt/velocity-manager-host/bin/velocity-manager start -s prometheus ')
  .then(function(result3) {
    sleep(10000)
    console.log(result3);
  })
})
sleep(10000)
ssh.connect({
  host: TARGET_IP,
  username: 'centos',
  privateKey: INSTANCE_KEY,
}).then(resp => {
  console.log('SUCCESS!!');
  //stop analytics
  ssh.execCommand('sudo nginx -s stop')
  .then(function(result1) {
    sleep(5000)
    console.log(result1);
  })
  ssh.execCommand('sudo nginx')
  .then(function(result1) {
    sleep(5000)
    console.log(result1);
  })
  ssh.execCommand('sudo /opt/cequence/bin/defender restart')
  .then(function(result1) {
    sleep(5000)
    console.log(result1);
  })
})
