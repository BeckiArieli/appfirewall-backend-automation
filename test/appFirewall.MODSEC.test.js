/***
 **
 **  MODSECURITY RULES TEST SUITE
 **
 **/

 'use strict'
 process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
 let { assert } = require('chai')
 let supertest = require('supertest');
 const { TARGET_IP } = require('../data/RULES_CONST')
 // let host = process.env.HOST;
 let api = supertest.agent('TARGET_IP');
 const sleep = (milliseconds) => {
   return new Promise(resolve => setTimeout(resolve, milliseconds))
 }

 describe.skip('MODSECURITY RULES TEST SUITE', () => {
   describe('MODSEC Rules BLOCK', () => {
     it('Schould block Accept-Lamnguage rule', async function(){
       await api.get('')
       .set('X-FORWARDED-FOR', '2.2.2.2')
       .set('HOST', 'live.emadison.com')
       .set("Accept-Language", "fr-CH")
       .expect(405)
       .then( (responce, reject) => {
           console.log('SUCCESS Custom rule 1', responce.status);
           assert(responce.status === 405)
       }).catch( err => {
           console.log('ERROR Custom rule 1', err);
       });
     })

     it('Should LOG', async function(){
       await api.get('')
       .set('X-FORWARDED-FOR', '33.33.33.33')
       .set('HOST', 'live.emadison.com')
       .set("X-OPT", "success")
       .expect(200)
       .then( (responce, reject) => {
           console.log('SUCCESS Custom rule 3', responce.status);
           assert(responce.status === 200)

       }).catch( err => {
           console.log('ERROR Custom rule 3', err);
       });
     })
     it('Should Block Multiple muches', async function(){
       await api.get('')
       .set('X-FORWARDED-FOR', '33.33.33.33')
       .set('HOST', 'live.emadison.com')
       .set("X-OPT", "success")
       .set('Cookie', ['myApp-token=12345667', 'myApp-other=blah'])
       .expect(457)
       .then( (responce, reject) => {
           console.log('SUCCESS Custom rule 4', responce.status);
           assert(responce.status === 457)

       }).catch( err => {
           console.log('ERROR Custom rule 4', err);
       });
     })
   })
   describe('MODSEC Rules HONEYTRAP', () => {
     it('Should honeytrap nikto header request', async function(){
       await api.get('')
       .set('X-FORWARDED-FOR', '2.2.2.2')
       .set('HOST', 'live.emadison.com')
       .set("X-OPT", "nikto")
       .expect(200)
       .then( (responce, reject) => {
           console.log('SUCCESS Custom rule 2', responce.status);
           assert(responce.status === 200)

       }).catch( err => {
           console.log('ERROR Custom rule 2', err);
       });
     })
   })
   describe('Custom Rules LOG', () => {
     it('Should LOG', async function(){
       await api.get('')
       .set('X-FORWARDED-FOR', '33.33.33.33')
       .set('HOST', 'live.emadison.com')
       .set("X-OPT", "success")
       .expect(200)
       .then( (responce, reject) => {
           console.log('SUCCESS Custom rule 3', responce.status);
           assert(responce.status === 200)

       }).catch( err => {
           console.log('ERROR Custom rule 3', err);
       });
     })
   })
   describe('MODSEC Rules ALLOW', () => {
       //TODO: Create ALLOW TESTS
       console.log('TODO');
   })
 })
