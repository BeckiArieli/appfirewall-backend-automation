/***
 **
 **  NEW TESTS TEST SUITE
 **
 **/

 'use strict'
 process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
 const assert = require('chai').assert;
 const curl = new (require( 'curl-request' ))();

 const supertest = require('supertest');
 const path = require('path');
 const { TARGET_IP, DETECTOR_IP } = require('../data/RULES_CONST')

 const api = supertest.agent(TARGET_IP);
 const data_path = path.resolve(__dirname, '..') + '/'

 const report_path = `${data_path}/mochawesome-report/mochawesome.html`
 const TESTS = require('../data/NEW_TESTS.json')

 const sleep = (milliseconds) => {
   return new Promise(resolve => setTimeout(resolve, milliseconds))
 }

 const getResults = async () => {
   const result_rules = []
   const firedRules = []
   const api_defender = supertest.agent(DETECTOR_IP);
   await api_defender.get(':9200/_search')
    .send('{"query": {"range": { "doc_timestamp": { "gte": "now-15m"}}}}')
    .then(res => {
        return res.body.hits
    }).then( hits => {
      for (const [i, v] of hits.hits.entries()) {
        const rule = v._source.rules
        if(rule){
          result_rules.push(...rule)
        }
      }
      for (var variable of result_rules) {
        if(!firedRules.includes(variable.id)){
          firedRules.push(variable.id)
        }
      }
    });
    return firedRules
 }

 after(async () => {
   sleep(3000)
   require("openurl").open(path.resolve(report_path))
 })

describe.skip('NEW TEST SUITE', () => {
  TESTS.forEach((item, i) => {
    it(item.name, async () =>{
      const status = 'OK'
      await api.get('')
      .set('HOST', 'live.emadisonland.com')
      .send({"var":"session.bug_compat_42%3dtrue"})
      .expect(403)
      .then(res => {
        console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Variables Found 933130', res.status);
        assert(res.status === 403)
      })
      sleep(1000)
    })
  });

})
