/***
 **
 **  MINIMUS ACCEPTANCE TEST
 **
 **/

'use strict'
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const assert = require('chai').assert;
const supertest = require('supertest');
const { TARGET_IP } = require('../data/RULES_CONST')
// const host = process.env.HOST;
let mitigator_type = process.env.MITIGATOR_TYPE
let api = supertest.agent(TARGET_IP);
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

console.log(TARGET_IP);
describe.skip('AppFirewall Minimum Acceptance Test Suite', () => {
  //** MAT OWASP RULES TEST ALLOW AND BLOCK
  describe('Owasp', () => {
    it('Should block this request based on IP', async () => {
      await api.get('')
        .set('X-FORWARDED-FOR', '1.1.1.1')
        .set('X-TEST', 'nikto')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP', res.status);
          assert(res.status === 403)
        })
    })
    it('Should block this request based on XSS INJECTION', async function() {
      await api.get('/demo/xss/xml/vuln.xml.php?input=<script xmlns=\"http://www.w3.org/1999/xhtml\">setTimeout(\"top.frame2.location=\\\"javascript:(function () {var x = document.createElement(\\\\\\\"script\\\\\\\");x.src = \\\\\\\"//sdl.me/popup.js?//\\\\\\\";document.childNodes\\[0\\].appendChild(x);}());\\\"\",1000)</script>&//')
        .set('X-FORWARDED-FOR', '1.1.1.1')
        .expect(403)
        .then(res => {
          console.log('SUCCESS OWASP', res.status);
          assert(res.status === 403)
        })
    })
  })

  // describe('Custom Rules', () => {
  //   //** MAT CUSTOM RULES RULES TEST ALLOW, BLOCK, HONEYTRAP, LOG
  //
  //   it('Should block Accept-Language rule', async function(){
  //     await api.get('')
  //     .set('X-FORWARDED-FOR', '2.2.2.2')
  //     .set('HOST', 'live.emadison.com')
  //     .set("Accept-Language", "fr-CH")
  //     .expect(444)
  //     .then( (response, reject) => {
  //         console.log('S Custom rule 1', response.status);
  //         assert(response.status === 444)
  //     })
  //   })
  //
  //   it('Should honeytrap nikto header request', async function(){
  //     await api.get('')
  //     .set('X-FORWARDED-FOR', '2.2.2.2')
  //     .set('HOST', 'live.emadison.com')
  //     .set("X-OPT", "nikto")
  //     .expect(200)
  //     .then( (response, reject) => {
  //         console.log('SUCCESS Custom rule 2', response.status);
  //         assert(response.status === 200)
  //     })
  //   }),
  //
  //   it('Should LOG', async function(){
  //     await api.get('')
  //     .set('X-FORWARDED-FOR', '33.33.33.33')
  //     .set('HOST', 'live.emadison.com')
  //     .set("X-OPT", "success")
  //     .expect(200)
  //     .then( (response, reject) => {
  //         console.log('SUCCESS Custom rule 3', response.status);
  //         assert(response.status === 200)
  //
  //     })
  //   })
  //   it('Should Block Multiple muches', async function(){
  //     await api.get('')
  //     .set('X-FORWARDED-FOR', '33.33.33.33')
  //     .set('HOST', 'live.emadison.com')
  //     .set("X-OPT", "success")
  //     .set('Cookie', ['myApp-token=12345667', 'myApp-other=blah'])
  //     .expect(445)
  //     .then( (response, reject) => {
  //         console.log('SUCCESS Custom rule 4', response.status);
  //         assert(response.status === 445)
  //     })
  //   })
  // })
  describe('Custom Rules', () => {
    it('Should Block Multiple muches', async function(){
      await api.get('')
      .set('X-FORWARDED-FOR', '33.33.33.33')
      .set('HOST', 'live.emadison.com')
      .set("X-OPT", "success")
      .set('Cookie', ['myApp-token=12345667', 'myApp-other=blah'])
      .expect(444)
      .then( (responce, reject) => {
          console.log('SUCCESS Custom rule 4', responce.status);
          assert(responce.status === 444)
      })
    })

    it('Should Block Cookie ', async function(){
      await api.get('')
      .set('X-FORWARDED-FOR', '33.33.33.33')
      .set('Cookie', ['myApp-token=12345667', 'myApp-other=blah'])
      .expect(454)
      .then( (responce, reject) => {
          console.log('SUCCESS Custom rule 4', responce.status);
          assert(responce.status === 454)

      })
    })

    xit('Should Block Country Code ', async function(){
      await api.get('')
      .set('X-FORWARDED-FOR', '33.33.33.33')
      .set('Country-code', 'AFG')
      .expect(403)
      .then( (responce, reject) => {
          console.log('SUCCESS Custom rule 4', responce.status);
          assert(responce.status === 403)

      })
    })
  })
})
